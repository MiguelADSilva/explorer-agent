package com.minderaschool.explorer.agent;

import com.minderaschool.explorer.client.BeaconMeasure;
import com.minderaschool.explorer.client.Robot;

public class MrRobot {
    public Robot robot;
    public String robName;

    private double center, left, right, compass;
    private BeaconMeasure beacon;
    private int ground;
    private boolean bumper;
    private int beaconToFollow;

    private Purpose currentPurpose;
    private Action currentAction;
    private enum Purpose {WAITING_TO_START, GO_CHEESE, HOME,GOHOME}
    private enum Action {WALK, ROTATE_90_LEFT, ROTATE_90_RIGHT}
    private boolean rotacaoBack = false;


    // Constructor
    public MrRobot(String robName, String host, int pos) {
        robot = new Robot();
        beacon = new BeaconMeasure();

        beaconToFollow = 0;
        ground = -1;

        System.out.println("Purpose: WAITING_TO_START");
        currentPurpose = Purpose.WAITING_TO_START;
        System.out.println("Action: WALK");
        currentAction = Action.WALK;

        this.robName = robName;

        // register robot in simulator
        robot.initRobot(robName, pos, host);
    }

    /**
     * reads a new message, decides what to do and sends action to simulator
     */

    public void mainLoop() {
        // waitingToStart();
        while (true) {
            robot.readSensors();
            decide(robot.getTime());
        }
    }

    public void waitingToStart(){
        double time;
        while(currentPurpose == Purpose.WAITING_TO_START) {
            robot.readSensors();
            time = robot.getTime();
            System.out.printf("[%f] [%s] [%s]: %f != %f = %s\n", time, currentPurpose, currentAction, time, 0.0, time!=0.0);
            if (time != 0.0) {
                System.out.printf("[%f] [%s] [%s]: Changing purpose to GO_CHEESE\n", time, currentPurpose, currentAction);
                currentPurpose = Purpose.GO_CHEESE;

            }
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void move(boolean follow){
    }

    /**
     * basic reactive decision algorithm, decides action based on current sensor values
     */
    public void decide(double time) {
        if (robot.isObstacleReady(0))
            center = robot.getObstacleSensor(0);
        if (robot.isObstacleReady(1))
            left = robot.getObstacleSensor(1);
        if (robot.isObstacleReady(2))
            right = robot.getObstacleSensor(2);

        if (robot.isCompassReady())
            compass = robot.getCompassSensor();
        if (robot.isBumperReady())
            bumper = robot.getBumperSensor();
        if (robot.isGroundReady())
            ground = robot.getGroundSensor();

        if (robot.isBeaconReady(beaconToFollow))
            beacon = robot.getBeaconSensor(beaconToFollow);

        System.out.println(currentAction);
        //System.out.println("Measures: ir0=" + irSensor0 + " ir1=" + left + " ir2=" + irSensor2 + "\n");
        switch (currentAction){
            case WALK:
                //System.out.printf("[%f] [%s] [%s]: Going forward (0.5, 0.5) \n", time, currentPurpose, currentAction);
                robot.driveMotors(0.5,0.5);

                //System.out.printf("[%f] [%s] [%s]: left [%f] Center [%f] Right [%f]\n", time, currentPurpose, currentAction, left, center, right);
                //If the center and left sensor encounters a wall the robot changes the state
                if(center >= 1.5 && left >= 1.5){
                    System.out.printf("[%f] [%s] [%s]: Stopping (0.0, 0.0) \n", time, currentPurpose, currentAction);
                    robot.driveMotors(0.5,-0.8);
                    System.out.printf("[%f] [%s] [%s]: Change Action to ROTATE_90_RIGHT \n", time, currentPurpose, currentAction);
                    currentAction = Action.ROTATE_90_RIGHT;
                }

                //If the center and right sensor encounters a wall the robot changes the state
                if(center >= 1.5 && right >= 1.5){
                    System.out.printf("[%f] [%s] [%s]: Stopping (0.0, 0.0) \n", time, currentPurpose, currentAction);
                    robot.driveMotors(-0.5,0.8);
                    System.out.printf("[%f] [%s] [%s]: Change Action to ROTATE_90_LEFT \n", time, currentPurpose, currentAction);
                    currentAction = Action.ROTATE_90_LEFT; // left
                }

                cheese(time);
                checkrotacao();


                if(center >= 2){
                    System.out.printf("[%f] [%s] [%s]: Stopping (0.0, 0.0) \n", time, currentPurpose, currentAction);
                    robot.driveMotors(-0.5,0.8);
                    System.out.printf("[%f] [%s] [%s]: Change Action to ROTATE_90_LEFT \n", time, currentPurpose, currentAction);
                    currentAction = Action.ROTATE_90_LEFT; // left
                }

                break;
            case ROTATE_90_LEFT:
                left90(time);
                currentAction = Action.WALK;
                break;
            case ROTATE_90_RIGHT:
                right90(time);
                currentAction = Action.WALK;
                break;
        }

        //System.out.println(robName + " state " + state);
    }


    //Method to turn left
    public void left90(double time){
        System.out.println("(ROTATE LEFT) This compass: " +compass);
        if(compass > 60  && compass < - 60){
            System.out.printf("[%f] [%s] [%s]: Rotating 90º Left \n", time, currentPurpose, currentAction);
            robot.driveMotors(0.0,0.0);
        }else{
            robot.driveMotors(-0.10,0.10);
        }
    }

    //Method to turn right
    public void right90(double time){
        System.out.println("(ROTATE RIGHT) This compass: " +compass);
        if(compass > 60 && compass < - 60){
            System.out.printf("[%f] [%s] [%s]: Rotating 90º Right \n", time, currentPurpose, currentAction);
            robot.driveMotors(0.0,0.0);
        }else{
            robot.driveMotors(0.10,-0.10);
        }
    }


   //Method to check if the mouse collides with the cheese
    public void cheese(double time){
        if(ground == 0){
            System.out.printf("[%f] [%s] [%s]: Eating", time, currentPurpose, currentAction);
            robot.setReturningLed(false);
            rotacaoBack = true;
            currentAction = Action.ROTATE_90_RIGHT;
        }
    }

    public void checkWalls(){

    }

    //check if the rotation is in true and if the measure is between those values ​​if it is then it moves forward
    public void  checkrotacao(){
        if(rotacaoBack == true){
            if(compass > 170 ||compass < -170){
                robot.driveMotors(0.2,0.2);
            }else{
                System.out.println("looking left");
                robot.driveMotors(-0.8,0.5);
            }
        }
    }

}


