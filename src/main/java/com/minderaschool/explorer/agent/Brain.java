package com.minderaschool.explorer.agent;

import com.minderaschool.explorer.client.BeaconMeasure;
import com.minderaschool.explorer.client.Robot;

public class Brain {
    public Robot robot;

    public String robName;
    private double center, left, right, compass;
    private BeaconMeasure beacon;

    private int ground;
    private boolean bumper;

    private State state;
    private int beaconToFollow;
    private enum State {RUN, WAIT, RETURN, GOHOME, ROTATE}


    // Constructor
    public Brain() {
        robot = new Robot();
        beacon = new BeaconMeasure();

        beaconToFollow = 0;
        ground = -1;

        state = State.RUN;
    }

    /**
     * reads a new message, decides what to do and sends action to simulator
     */
    public void mainLoop() {

        while (true) {
            robot.readSensors();
            decide();

        }
    }

    public void moveAvoid(boolean followBeacon){
        if(center > 2 || left > 2.3 || right > 2.3){
            if(left > right){
                robot.driveMotors(0.0,-0.08);
                if(center > 1.8 && left >1.9 &&right > 1.9){
                    robot.driveMotors(-0.15,+0.15);
                }
            }else{
                robot.driveMotors(-0.05,0.0);
            }
        }else{
            robot.driveMotors(0.15,0.15);
        }
    }



    /**
     * basic reactive decision algorithm, decides action based on current sensor values
     */
    public void decide() {
        if (robot.isObstacleReady(0))
            center = robot.getObstacleSensor(0);
        if (robot.isObstacleReady(1))
            left = robot.getObstacleSensor(1);
        if (robot.isObstacleReady(2))
            right = robot.getObstacleSensor(2);

        if (robot.isCompassReady())
            compass = robot.getCompassSensor();
        if (robot.isBumperReady())
            bumper = robot.getBumperSensor();
        if (robot.isGroundReady())
            ground = robot.getGroundSensor();

        if (robot.isBeaconReady(beaconToFollow))
            beacon = robot.getBeaconSensor(beaconToFollow);

        //System.out.println("Measures: ir0=" + irSensor0 + " ir1=" + left + " ir2=" + irSensor2 + "\n");

        //System.out.println(robName + " state " + state);

        switch (state) {
            case RUN:    /* Go */
                System.out.println("Running");
                if(ground == 0){
                    robot.setVisitingLed(true);
                    state = State.ROTATE;
                }else{
                    moveAvoid(false);
                }
                break;

            case WAIT: /* set returning led and check that it is on */
                System.out.println("Waiting");
                robot.setReturningLed(true);
                if (robot.getVisitingLed()) robot.setVisitingLed(false);
                if (robot.getReturningLed()) state = State.RETURN;

                robot.driveMotors(0.0, 0.0);
                break;

            case GOHOME:
                System.out.println("GoHome");
                robot.setReturningLed(true);
                moveAvoid(false);
                break;
            case ROTATE:
                System.out.println("Rotate");
                if(compass > 170 ||compass < -170){
                    System.out.println("Looking back");
                    robot.driveMotors(0.0,0.0);
                    state = State.GOHOME;
                }else{
                    System.out.println("looking left");
                    robot.driveMotors(-0.8,0.5);
                }
                break;
            case RETURN: /* Return to home area */
                System.out.println("Returning");
                robot.setVisitingLed(false);
                robot.setReturningLed(false);
                moveAvoid(false);
                break;

        }


        //for(int i=1; i<6; i++)
        //  if(robot.NewMessageFrom(i))
        //      System.out.println("Message: From " + i + " to " + robName + " : \"" + robot.GetMessageFrom(i)+ "\"");

        //robot.Say(robName);

        if (robot.getTime() % 2 == 0) {
            robot.requestIRSensor(0);
            robot.requestIRSensor(1);
            robot.requestIRSensor(2);
            robot.requestGroundSensor();
        } else {
            String[] sensList = {"left", "Right", "Compass", "GPS"};
            robot.requestSensors(sensList);
        }
    }
  }
